#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# no dependencies except python3 itself
import os
import sys
import json
import urllib.request
import urllib.parse

def https_post(*url_bits, headers = {}, params = {}):
    with urllib.request.urlopen(
        urllib.request.Request(
            "https://" + "/".join(urllib.parse.quote_plus(bit) for bit in url_bits),
            method = "POST",
            headers = headers,
            data = urllib.parse.urlencode(params).encode("ASCII")
        )
    ) as request:
        return request.read()

def ci_comment_on_commit(commit_hash, text):
    return json.loads(
        https_post(
            "framagit.org", "api", "v4", "projects", os.environ["CI_PROJECT_PATH"], "repository", "commits", commit_hash, "comments",
            headers = { "PRIVATE-TOKEN": os.environ["gitlab_api_token"] },
            params = { "note": text }
        )
    )["note"] == text.strip()

def markdown(text):
    return text.replace("\n", "  \n")

def comment_posted(ci_message):
    if ci_message == "":
        raise Exception("Cannot submit an empty comment!")

    return ci_comment_on_commit(
        commit_hash = os.environ["CI_COMMIT_SHA"],
        text = markdown(
			"**ℹ️ If you have not finished submitting your contribution, please ignore this message.**\n"
			"\n"
			"It's probably an easy fix, but as of this commit, automatic checks pointed out the following:\n"
			"\n"
			+ ci_message + "\n"
			"\n"
			"**❓ If you can't fix this, or would like explanations, just comment and we'll help!"
			" You can also ask on [IRC](https://www.peppercarrot.com/static4/community#irc).**"
		)
    )

exit(0 if comment_posted(sys.stdin.read().strip()) else 1)
