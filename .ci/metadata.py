#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# no dependencies except python3 itself
import sys
from glob import glob
from os import path, environ
from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.sax.saxutils import quoteattr
import re
import json

from typing import Dict, Iterable, List, Optional, TypeVar, Union
a = TypeVar('a')


class PredicateFailure(Exception):
	pass

class FileIssue(PredicateFailure):
	def __init__(self, *, issue: str, filepath: str, message: str, help: str):
		Exception.__init__(
			self,
			{
				'issue':    issue,
				'filepath': filepath,
				'message':  message,
				'help':     help
			}
		)
	def __str__(self) -> str:
		return (
			'{issue} in "{filepath}": {message}.\n'
			'{help}'
		).format(**self.args[0])

class MissingElement(FileIssue):
	def __init__(self, *, filepath: str, message: str, help: str):
		FileIssue.__init__(
			self,
			issue    = "Element not found",
			filepath = filepath,
			message  = message,
			help     = help
		)

class Inconsistency(PredicateFailure):
	def __init__(self, *, issue: str, path1: str, item1: str, path2: str, item2: str, help: str):
		Exception.__init__(
			self,
			{
				'issue': issue,
				'path1': path1, 'item1': item1,
				'path2': path2, 'item2': item2,
				'help':  help
			}
		)
	def __str__(self) -> str:
		return (
			'{issue}:\n'
			'"{item1}" in "{path1}", and\n'
			'"{item2}" in "{path2}".\n'
			'{help}'
		).format(**self.args[0])


def raise_if_none(value: Optional[a], exception: Exception) -> a:
	if value is None:
		raise exception
	else:
		return value

def first(iterable: Iterable[a]) -> Optional[a]:
	return next(iter(iterable), None)

def raise_if_multiple(list: List[a], exception: Exception) -> Optional[a]:
	if len(list) > 1:
		raise exception
	else:
		return first(list)

def concat(lists: Iterable[List[a]]) -> List[a]:
	"""The contatenation of the lists"""
	return sum(lists, [])


def globber(*pattern_path: str) -> List[str]:
	"""A list of all the normalized paths expanded with glob from the reassembled path pattern"""
	return [
		path.normpath(p)
		for p
		in glob( path.join(*pattern_path) )
	]

def xml_subtrees(xml_filepath: str, element_id: str) -> List[Element]:
	"""The subtree roots having the specified ID
	   Raises FileIssue if the file cannot be parsed as XML"""
	try:
		# Note: escaping isn't supported by findall, so element_id can't contain quotes
		return ElementTree.parse(xml_filepath).findall(".//*[@id='" + element_id + "']")
	except ElementTree.ParseError as parse_error:
		raise FileIssue(
			issue    = "Syntax error",
			filepath = xml_filepath,
			message  = parse_error.args[0],
			help     = "The file could be truncated or corrupt, or it could contain garbage"
			           " due to an incomplete git merge. If it was edited by hand, some escaping"
			           " could be missing."
		)

def maybe_xml_subtree(xml_filepath: str, element_id: str) -> Optional[Element]:
	"""The subtree whose root is has the ID element_id, or None if not found
	   Raises FileIssue if the subtree is duplicated"""
	return raise_if_multiple(
		xml_subtrees(xml_filepath, element_id),
		FileIssue(
			issue    = "Duplicate element IDs",
			filepath = xml_filepath,
			message  = "several elements with id=" + quoteattr(element_id),
			help     = "This is not allowed to happen in XML. The file must have been"
			           " edited by hand, or has been broken by an incomplete git merge."
		)
	)

def xml_subtree(xml_filepath: str, element_id: str) -> Element:
	"""The subtree whose root is has the ID element_id
	   Raises MissingElement if the subtree isn't found, or FileIssue if duplicated"""
	return raise_if_none(
		maybe_xml_subtree(xml_filepath, element_id),
		MissingElement(
			filepath = xml_filepath,
			message  = "no element with id=" + quoteattr(element_id),
			help     = "This element might have been deleted and re-created with a different id."
		)
	)

def xml_element_text(xml_filepath: str, element_id: str) -> str:
	"""The concatenation of all the text in the element whose ID is element_id"""
	return "".join(
		text
		for text
		in concat(
			[ element.text, element.tail ]
			for element
			in xml_subtree(xml_filepath, element_id).iter()
		)
		if text is not None
	)


def episode_title(svg_filepath: str) -> str:
	"""The name of the episode found in the SVG file at this path
	   Raises FileIssue if the title isn't found"""
	try:
		return xml_element_text(svg_filepath, element_id = "episode-title").strip()
	except MissingElement:
		raise FileIssue(
			issue    = "Title not found",
			filepath = svg_filepath,
			message  = 'no SVG element with id="episode-title"',
			help     = "Open the file in Inkscape, select the text element of the title,"
			           " click 'Edit -> XML Editor', click 'id' under 'Name', change what's in"
			           " the field at the bottom to 'episode-title', click 'Set' and save."
		)

def expected_titlepage_label(coverpage_label: str, language: str) -> str:
	"""What the title page label should be for this language, deduced from the cover page label"""
	cover_to_title = {
		"jb": ("^ni'o ", ".i "),
		None: ("", "")
	}
	return re.sub(
		*cover_to_title.get(language, cover_to_title[None]),
		string = coverpage_label
	)

def same_or_none(item1, item2):
	"""The value of both items if it is identical, otherwise None"""
	return item1 if item1 == item2 else None

def consistent_episode_title(coverpage_path: str, titlepage_path: str, language: str) -> str:
	"""The title label on the cover page and the title page
	   Raises Inconsistency if the titles don't match"""

	coverpage_label = episode_title(coverpage_path)
	titlepage_label = episode_title(titlepage_path)

	return raise_if_none(
		same_or_none(
			expected_titlepage_label(coverpage_label, language),
			titlepage_label
		),
		Inconsistency(
			issue = "Cover label and title label don't match",
			path1 = coverpage_path, item1 = coverpage_label,
			path2 = titlepage_path, item2 = titlepage_label,
			help  = "Please use the same title on the title page and the cover page"
			        " (including spaces, accents and capitalization)."
		)
	)


# WIP - TODO: don't glob, parse filenames
def glob_filename(*pattern_path: str) -> str:
	# returns the first filename matching the glob pattern
	# raises an exception if no file is found
	return raise_if_none(
		raise_if_multiple(
			globber(*pattern_path),
			FileIssue(
				issue    = "Too many files",
				filepath = path.join(*pattern_path),
				message  = "the wildcard pattern matched several files",
				help     = "You have a lingering file whose filename also matches this glob."
			)
		),
		FileIssue(
			issue    = "File not found",
			filepath = path.join(*pattern_path),
			message  = "the wildcard pattern didn't match any file",
			help     = "Please check that the file is present and correctly named."
		)
	)

def episode_languages(episode_dir: str) -> List[str]:
	"""The names of all directories in the 'lang' directory of this episode"""
	return [
		path.basename(lang_dir)
		for lang_dir
		in sorted( globber(episode_dir, 'lang', '*/') )
	]

def streamexcept(attempt, exception, stream = sys.stderr):
	"""The input or the specified exception, if raised
	   Prints that potential exception, to stderr by default"""
	try:
		return attempt()
	except exception as err:
		try:
			stream.touched
			print("", file = stream)
		except AttributeError:
			stream.touched = None
		print(err, file = stream)
		return err

def episode_native_titles(episode_dir: str) -> Dict[str, Union[str, PredicateFailure]]:
	"""Dictionary of language => native title for this episode"""
	return {
		episode_language: streamexcept(
			lambda: consistent_episode_title(
				coverpage_path = glob_filename(episode_dir, 'lang', episode_language, 'E??.svg'),
				titlepage_path = glob_filename(episode_dir, 'lang', episode_language, 'E??P00.svg'),
				language = episode_language
			),
			exception = PredicateFailure
		)
		for episode_language
		in episode_languages(episode_dir)
	}

def episodes_metadata(webcomics_dir: str) -> List[Dict]:
	"""Metadata for all episodes in this webcomics directory"""
	return [
		{
			"directory":    path.basename(episode_dir),
			"translations": episode_native_titles(episode_dir),
		}
		for episode_dir
		in sorted( globber(webcomics_dir, 'ep*_*/') )
	]

if __name__ == '__main__':
	if(len(sys.argv) < 2):
		print("Required parameter: path to webcomics directory")
		exit(2) # Argument error

	try:
		# The episodes' metadata generated from the episodes directory,
		# that we take as the first argument of the script
		metadata = episodes_metadata(webcomics_dir = sys.argv[1])
		print(
			json.dumps(
				metadata,
				indent = 4,
				# Maybe not ASCII-armoring the Unicode (looks nicer for tests)
				ensure_ascii = environ.get("UNICODE_JSON") is None
			)
		)
		exit(0) # Normal exit, everything fine
	except TypeError:
		exit(42) # Normal exit, we reported trouble

	exit(-1) # Panic catchall
